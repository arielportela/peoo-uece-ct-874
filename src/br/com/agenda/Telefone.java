package br.com.agenda;

public class Telefone {
	private String numero;
	private String codigo_area;
	private String operadora;
	
	public Telefone(String numero, String codigo_area, String operadora) {
		super();
		this.numero = numero;
		this.codigo_area = codigo_area;
		this.operadora = operadora;
	}
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getCodigo_area() {
		return codigo_area;
	}
	public void setCodigo_area(String codigo_area) {
		this.codigo_area = codigo_area;
	}
	public String getOperadora() {
		return operadora;
	}
	public void setOperadora(String operadora) {
		this.operadora = operadora;
	}
	
	public String getNumeroCompleto(){
		return codigo_area + numero;
	}
}
