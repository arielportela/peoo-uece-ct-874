package br.com.agenda;

import java.time.LocalDate;
import java.time.MonthDay;
import java.time.Year;
import java.time.temporal.ChronoUnit;

public class Data {
	private LocalDate data;
	
	public Data(int dia, int mes, int ano){
		data = LocalDate.of(ano, mes, dia);
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(int dia, int mes, int ano) {
		data = LocalDate.of(ano, mes, dia);
	}
	
	public long getDiasProximoAniversario(){
		MonthDay diaAniversario = MonthDay.of(data.getMonth(), data.getDayOfMonth());
		MonthDay hoje = MonthDay.now();
		LocalDate proxaniversario;
		if(diaAniversario.compareTo(hoje) < 0){// o aniversário desse ano já passou, só ano que vem agora
			proxaniversario = diaAniversario.atYear(Year.now().getValue() + 1);
		}else{ // ainda tem aniversário nesse ano!
			proxaniversario = diaAniversario.atYear(Year.now().getValue());
		}
		return LocalDate.now().until(proxaniversario, ChronoUnit.DAYS);
	}
	
	public long getDiasPara(){
		return LocalDate.now().until(data, ChronoUnit.DAYS);
	}
	
}
