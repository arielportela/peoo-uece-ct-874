package br.com.agenda;

import java.util.ArrayList;

public class Contato {
	protected String nome;
	protected ArrayList<Telefone> telefones;
	protected Endereco endereco;
	protected ArrayList<String> emails;
	protected Data aniversario;
	
	public Contato(String nome, Telefone telefone, Endereco endereco, String email, Data aniversario){
		telefones = new ArrayList<Telefone>();
		emails = new ArrayList<String>();
		this.nome = nome;
		telefones.add(telefone);
		this.endereco = endereco;
		emails.add(email);
		this.aniversario = aniversario;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public ArrayList<Telefone> getTelefones() {
		return telefones;
	}
	public void addTelefone(Telefone telefone){
		telefones.add(telefone);
	}
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public ArrayList<String> getEmails() {
		return emails;
	}
	public void addEmail(String email){
		emails.add(email);
	}
	public Data getAniversario() {
		return aniversario;
	}
	public void setAniversario(Data aniversario) {
		this.aniversario = aniversario;
	}
	public long getDiasProximoAniversario(){
//		MonthDay diaAniversario = MonthDay.of(aniversario.getMonth(), aniversario.getDayOfMonth());
//		MonthDay hoje = MonthDay.now();
//		LocalDate proxaniversario;
//		if(diaAniversario.compareTo(hoje) < 0){// o aniversário desse ano já passou, só ano que vem agora
//			proxaniversario = diaAniversario.atYear(Year.now().getValue() + 1);
//		}else{ // ainda tem aniversário nesse ano!
//			proxaniversario = diaAniversario.atYear(Year.now().getValue());
//		}
//		return LocalDate.now().until(proxaniversario, ChronoUnit.DAYS);
		return aniversario.getDiasProximoAniversario();
	}
	
}
