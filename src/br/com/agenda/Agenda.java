package br.com.agenda;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Agenda {
	
	static ArrayList<Contato> contatos;

	public static void main(String[] args) {
		contatos = new ArrayList<Contato>();
		System.out.println("---- Agenda ----");
		Scanner scanner = new Scanner(System.in);
		int choice = 0;
		
		while(choice != 3){
			System.out.println("Escolha uma opcao");
			System.out.println("1 - Adicionar contato");
			System.out.println("2 - Exibir contatos");
			System.out.println("3 - Sair");
			choice = scanner.nextInt();
			switch(choice){
				case 1:
					addContact();
					break;
				case 2:
					showAllContacts();
					break;
				case 3:
					scanner.close();
					break;
				default:
					System.out.println("Default");
			}
		}
		
	}
	
	public static void addContact(){
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Contato contato;
		try {
			System.out.print("Digite o nome do contato: ");
			String nome = br.readLine();
			
			System.out.print("\n\nDigite o nome da rua: ");
			String rua = br.readLine();
			System.out.print("\nDigite o número da rua: ");
			String numero = br.readLine();
			System.out.print("\nDigite o complemento da rua: ");
			String complemento = br.readLine();
			System.out.print("Digite o bairro da rua: ");
			String bairro = br.readLine();
			System.out.print("\nDigite o cep da rua: ");
			String cep = br.readLine();
			System.out.print("\nDigite a cidade: ");
			String cidade = br.readLine();
			System.out.print("\nDigite o estado: ");
			String estado = br.readLine();
			System.out.print("\nDigite o pais: ");
			String pais = br.readLine();
			Endereco endereco = new Endereco(rua, numero, complemento, bairro, cep, cidade, estado, pais);
			
			System.out.print("\n\nDigite o telefone: ");
			String numero_telefone = br.readLine();
			System.out.print("\nDigite o código de área: ");
			String codigo_area = br.readLine();
			System.out.print("\nDigite a operadora: ");
			String operadora = br.readLine();
			Telefone telefone = new Telefone(numero_telefone, codigo_area, operadora);
			
			System.out.print("\n\nDigite o email do contato: ");
			String email = br.readLine();
			
			System.out.println("\n\nDigite a data de nascimento");
			System.out.print("\nDia (numero): ");
			int dia = Integer.parseInt(br.readLine());
			System.out.print("\nMes (numero): ");
			int mes = Integer.parseInt(br.readLine());
			System.out.print("\nAno (numero): ");
			int ano = Integer.parseInt(br.readLine());
			Data data = new Data(dia, mes, ano);
			
			System.out.println("\n\nDigite o grupo do contato");
			System.out.println("\n1 - Amigo");
			System.out.println("2 - Familia");
			System.out.println("3 - Trabalho");
			
			int tipo = Integer.parseInt(br.readLine());
			if(tipo == 1 || tipo == 3){
				System.out.println("\n\nDigite uma reuniao");
				System.out.print("Nome da reuniao: ");
				String nome_reuniao = br.readLine();
				System.out.print("\nDia (numero): ");
				int dia_reuniao = Integer.parseInt(br.readLine());
				System.out.print("\nMes (numero): ");
				int mes_reuniao = Integer.parseInt(br.readLine());
				System.out.print("\nAno (numero): ");
				int ano_reuniao = Integer.parseInt(br.readLine());
				Data data_reuniao = new Data(dia_reuniao, mes_reuniao, ano_reuniao);
				System.out.print("\nHorario: ");
				String hora_reuniao = br.readLine();
				Reuniao reuniao = new Reuniao(nome_reuniao,hora_reuniao,data_reuniao);
				
				if(tipo == 3){
					System.out.println("\n\nDigite a empresa");
					System.out.print("Nome da empresa: ");
					String nome_empresa = br.readLine();
					System.out.print("\nSetor: ");
					String setor = br.readLine();
					
					Empresa empresa = new Empresa(nome_empresa, setor);
					
					contato = new Trabalho(nome, telefone, endereco, email, data, empresa, reuniao);
					contatos.add(contato);
					return;
				}else{
					contato = new Amigo(nome, telefone, endereco, email, data, reuniao);
					contatos.add(contato);
					return;
				}
			}else if(tipo == 2){
				System.out.println("\n\nDigite o parentesco do contato");
				String parentesco = br.readLine();
				contato = new Familia(nome, telefone, endereco, email, data, parentesco);
				contatos.add(contato);
				return;
			}else{
				System.out.println("Tipo invalido");
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Falha ao ler dados do console");
			e.printStackTrace();
		}
	}
	
	public static void showAllContacts(){
		System.out.println("ID  -   Nome  -  Telefone - Dias para aniversario");
		for(int i = 0; i < contatos.size(); i++){
			for(Telefone telefone: contatos.get(i).telefones){
				System.out.println(i + " - " + contatos.get(i).nome + " - " + telefone + " - " + contatos.get(i).getDiasProximoAniversario());
			}
		}
	}
	
	public static void editContact(){
		
	}
	
	public static void removeContact(){
		
	}

}
