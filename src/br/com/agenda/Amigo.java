package br.com.agenda;

import java.util.ArrayList;

public class Amigo extends Contato {
	
	private ArrayList<Reuniao> reunioes;

	public Amigo(String nome, Telefone telefone, Endereco endereco, String email, Data aniversario, Reuniao reuniao) {
		super(nome, telefone, endereco, email, aniversario);
		reunioes = new ArrayList<Reuniao>();
	}
	
	public ArrayList<Reuniao> getReunioes() {
		return reunioes;
	}

	public void addReuniao(Reuniao reuniao) {
		reunioes.add(reuniao);
	}
	
	

	
	
}
