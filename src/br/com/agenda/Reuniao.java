package br.com.agenda;

public class Reuniao {

	private String nome;
	private String horario;
	private Data data;
	
	public Reuniao(String nome, String horario, Data data) {
		super();
		this.nome = nome;
		this.horario = horario;
		this.data = data;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getHorario() {
		return horario;
	}
	public void setHorario(String horario) {
		this.horario = horario;
	}
	public Data getData() {
		return data;
	}
	public void setData(Data data) {
		this.data = data;
	}
	
	public String getDiasParaReuniao(){
		long dias = data.getDiasPara();
		String diasFaltando;
		if(dias < 0){
			diasFaltando = "A reuniao ja ocorreu";
		}else if(dias == 0){
			diasFaltando = "A reuniao sera hoje";
		}else{
			diasFaltando = "" + dias;
		}
		
		return diasFaltando;
	}
	
	
	
}
