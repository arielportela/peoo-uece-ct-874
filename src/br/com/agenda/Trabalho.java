package br.com.agenda;

import java.util.ArrayList;

public class Trabalho extends Contato{
	
	private ArrayList<Reuniao> reunioes;
	private Empresa empresa;
	
	public Trabalho(String nome, Telefone telefone, Endereco endereco, String email, Data aniversario, Empresa empresa, Reuniao reuniao) {
		super(nome, telefone, endereco, email, aniversario);
		reunioes = new ArrayList<Reuniao>();
		reunioes.add(reuniao);
		this.empresa = empresa;
	}
	
	public ArrayList<Reuniao> getReunioes() {
		return reunioes;
	}

	public void addReuniao(Reuniao reuniao) {
		reunioes.add(reuniao);
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	



}
