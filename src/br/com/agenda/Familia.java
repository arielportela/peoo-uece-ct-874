package br.com.agenda;

public class Familia extends Contato{
	
	private String parentesco;

	public Familia(String nome, Telefone telefone, Endereco endereco, String email, Data aniversario, String parentesco) {
		super(nome, telefone, endereco, email, aniversario);
		this.parentesco = parentesco;
	}
	
	public String getParentesco() {
		return parentesco;
	}

	public void setParentesco(String parentesco) {
		this.parentesco = parentesco;
	}

}
