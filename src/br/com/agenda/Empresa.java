package br.com.agenda;

public class Empresa {

	private String nome;
	private String setor;
	
	public Empresa(String nome, String setor) {
		super();
		this.nome = nome;
		this.setor = setor;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSetor() {
		return setor;
	}
	public void setSetor(String setor) {
		this.setor = setor;
	}
	
	
}
